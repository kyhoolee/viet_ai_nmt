#!/bin/bash

# ./run_train_multiple_option.sh > log_train_multiple 2>&1 &


#######################################################################
# SGD với learning rate 1.0 - default model


# # 1. adams với learning rate 0.001.
# rm -rf ./tmp/nmt_attention_model_scaled_luong_adams
# mkdir ./tmp/nmt_attention_model_scaled_luong_adams

# python3 -m nmt.nmt \
#     --attention=scaled_luong \
#     --src=vi --tgt=en \
#     --vocab_prefix=./tmp/nmt_data/vocab  \
#     --train_prefix=./tmp/nmt_data/train \
#     --dev_prefix=./tmp/nmt_data/tst2012  \
#     --test_prefix=./tmp/nmt_data/tst2013 \
#     --out_dir=./tmp/nmt_attention_model_scaled_luong_adams \
#     --num_train_steps=12000 \
#     --steps_per_stats=100 \
#     --num_layers=2 \
#     --num_units=128 \
#     --dropout=0.2 \
#     --optimizer=adam \
#     --learning_rate=0.001 \
#     --metrics=bleu



# # 2. scaled_luong + bidirectional + greedy
# rm -rf ./tmp/nmt_attention_model_scaled_luong_bidirectional
# mkdir ./tmp/nmt_attention_model_scaled_luong_bidirectional

# python3 -m nmt.nmt \
#     --attention=scaled_luong \
#     --src=vi --tgt=en \
#     --vocab_prefix=./tmp/nmt_data/vocab  \
#     --train_prefix=./tmp/nmt_data/train \
#     --dev_prefix=./tmp/nmt_data/tst2012  \
#     --test_prefix=./tmp/nmt_data/tst2013 \
#     --out_dir=./tmp/nmt_attention_model_scaled_luong_bidirectional \
#     --num_train_steps=12000 \
#     --steps_per_stats=100 \
#     --num_layers=2 \
#     --num_units=128 \
#     --dropout=0.2 \
#     --encoder_type=bi \
#     --metrics=bleu



# "--infer_mode", ["greedy", "sample", "beam_search"],
# 3. scaled_luong + bidirectional + beam_search
rm -rf ./tmp/nmt_attention_model_scaled_luong_bidirectional_beam_search
mkdir ./tmp/nmt_attention_model_scaled_luong_bidirectional_beam_search

python3 -m nmt.nmt \
    --attention=scaled_luong \
    --src=vi --tgt=en \
    --vocab_prefix=./tmp/nmt_data/vocab  \
    --train_prefix=./tmp/nmt_data/train \
    --dev_prefix=./tmp/nmt_data/tst2012  \
    --test_prefix=./tmp/nmt_data/tst2013 \
    --out_dir=./tmp/nmt_attention_model_scaled_luong_bidirectional_beam_search \
    --num_train_steps=12000 \
    --steps_per_stats=100 \
    --num_layers=2 \
    --num_units=128 \
    --dropout=0.2 \
    --encoder_type=bi \
    --infer_mode=beam_search \
    --beam_width=10 \
    --metrics=bleu




# 4. normed_bahdanau + bidirectional + beam_search
rm -rf ./tmp/nmt_attention_model_normed_bahdanau_bidirectional_beam_search
mkdir ./tmp/nmt_attention_model_normed_bahdanau_bidirectional_beam_search

python3 -m nmt.nmt \
    --attention=normed_bahdanau \
    --src=vi --tgt=en \
    --vocab_prefix=./tmp/nmt_data/vocab  \
    --train_prefix=./tmp/nmt_data/train \
    --dev_prefix=./tmp/nmt_data/tst2012  \
    --test_prefix=./tmp/nmt_data/tst2013 \
    --out_dir=./tmp/nmt_attention_model_normed_bahdanau_bidirectional_beam_search \
    --num_train_steps=12000 \
    --steps_per_stats=100 \
    --num_layers=2 \
    --num_units=128 \
    --dropout=0.2 \
    --encoder_type=bi \
    --infer_mode=beam_search \
    --beam_width=10 \
    --metrics=bleu

