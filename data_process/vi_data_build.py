from unidecode import unidecode


def unaccent_file(in_file, out_file):
    out = open(out_file, 'w+')
    f = open(in_file, 'r')
    for line in f:
        unaccent = un_accent(line)
        out.write(unaccent)

    f.close()
    out.close()

def unaccent_vocab(in_file, out_file):
    out = open(out_file, 'w+')
    f = open(in_file, 'r')
    vocab = set()
    for line in f:
        unaccent = un_accent(line)
        if unaccent not in vocab:
            out.write(unaccent)
            vocab.add(unaccent)

    f.close()
    out.close()


def un_accent(s):
    result = unidecode(s).encode('ascii')
    return result.decode("utf-8")

if __name__ == "__main__":
    #unaccent_file('../tmp/nmt_data/train.vi', '../tmp/nmt_data/train.ac')
    unaccent_vocab('../tmp/nmt_data/tst2012.vi', '../tmp/nmt_data/tst2012.ac')
    unaccent_vocab('../tmp/nmt_data/tst2013.vi', '../tmp/nmt_data/tst2013.ac')