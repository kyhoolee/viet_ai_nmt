 #!/bin/bash
python3 -m nmt.nmt \
    --out_dir=./tmp/nmt_attention_model \
    --inference_input_file=./tmp/sample_infer.vi \
    --inference_output_file=./tmp/nmt_attention_model/output_attention_infer


python3 -m nmt.nmt \
	--out_dir=./tmp/nmt_model \
	--inference_input_file=./tmp/sample_infer.vi \
	--inference_output_file=./tmp/nmt_model/output_infer