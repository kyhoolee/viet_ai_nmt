#!/bin/bash
rm -rf ./tmp/accent_nac
mkdir ./tmp/accent_nac

python3 -m nmt.nmt \
    --attention=scaled_luong \
    --src=ac --tgt=vi \
    --vocab_prefix=./tmp/nmt_r_data/vocab  \
    --train_prefix=./tmp/nmt_r_data/train \
    --dev_prefix=./tmp/nmt_r_data/tst2012  \
    --test_prefix=./tmp/nmt_r_data/tst2013 \
    --out_dir=./tmp/accent_nac \
    --num_train_steps=12000 \
    --steps_per_stats=100 \
    --num_layers=2 \
    --num_units=128 \
    --dropout=0.2 \
    --encoder_type=bi \
    --infer_mode=beam_search \
    --beam_width=10 \
    --metrics=bleu

